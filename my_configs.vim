 " Plug
 call plug#begin('~/.vim/plugged')
 Plug 'tidalcycles/vim-tidal'
 Plug 'mcchrish/nnn.vim'
 Plug 'stevearc/vim-arduino'
 Plug 'chriskempson/base16-vim'
 Plug 'arcticicestudio/nord-vim'
 Plug 'ayu-theme/ayu-vim'
 Plug 'christoomey/vim-system-copy'
 call plug#end()
 " nerdtree
 autocmd StdinReadPre * let s:std_in=1
 autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
 autocmd StdinReadPre * let s:std_in=1
 autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && v:this_session == "" | NERDTree | endif
 autocmd StdinReadPre * let s:std_in=1
 autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree'
 argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
 nnoremap <C-e> :NERDTreeToggle<CR>
 nnoremap <silent> <expr> <F6> g:NERDTree.IsOpen() ? "\:NERDTreeClose<CR>" :
 bufexists(expand('%')) ? "\:NERDTreeFind<CR>" : "\:NERDTree<CR>"
 "stuff
 filetype plugin indent on
 " On pressing tab, insert 2 spaces
 set expandtab
 " show existing tab with 2 spaces width
 set tabstop=2
 set softtabstop=2
 " when indenting with '>', use 2 spaces width
 set shiftwidth=2
 "copy paste
 :set clipboard=unnamedplus
 vnoremap <C-c> "*y
 " theme
 "colorscheme base16-atelier-forest
 "colorscheme nord
 let base16colorspace=256
 set termguicolors
 "let ayucolor="mirage"
 "colorscheme ayu
 colorscheme nord

 " lines lenght
 :set wrap
 :set linebreak
 :set textwidth=0
 :set wrapmargin=0
" tidal
 let maplocalleader=","
 filetype plugin indent on
 let g:tidal_target = "terminal"
 " Arduino
 nnoremap <buffer> <leader>am :ArduinoVerify<CR>
 nnoremap <buffer> <leader>au :ArduinoUpload<CR>
 nnoremap <buffer> <leader>ad :ArduinoUploadAndSerial<CR>
 nnoremap <buffer> <leader>ab :ArduinoChooseBoard<CR>
 nnoremap <buffer> <leader>ap :ArduinoChooseProgrammer<CR>
